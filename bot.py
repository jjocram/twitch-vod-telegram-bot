from os import environ

from telegram import ReplyKeyboardMarkup
from telegram import ReplyKeyboardRemove
from telegram import Update
from telegram.ext import CallbackContext
from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater
from tvod import get_video_info
from tvod import get_video_url

RESOLUTION = range(1)
resolutions = {
    "360p60":"360p60",
    "720p60":"720p60"
}

def start(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Welcome send me a twitch vod link or a video ID")

def fail_callback():
    return None, None

def start_get_video_url(update: Update, context: CallbackContext):
    message = update.message.text

    if message.isdigit():
        vod_id = message
    else:
        vod_id = message.split("/")[-1]
    animated_preview_url = get_video_info(vod_id, fail_callback)
    if not animated_preview_url:
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text="Cannot find this video")
        return ConversationHandler.END
    reply_keyboard = [[k for k in resolutions]]
    context.user_data['animated_preview_url'] = animated_preview_url
    update.message.reply_text("Select the resolution:",
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                               one_time_keyboard=True,
                                                               input_field_placeholder="Resolution"))
    return RESOLUTION

def get_video(update: Update, context: CallbackContext):
    resolution = update.message.text
    animated_preview_url = context.user_data.get("animated_preview_url")
    vod_url = get_video_url(animated_preview_url, resolution)
    update.message.reply_text(f"{vod_url}", reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END

def cancel(update: Update, context: CallbackContext):
    return ConversationHandler.END

def main():
    # Comment line to change if bot is stuck: 0
    if token := environ.get("bot_token", None):
        port = int(environ.get("PORT", 8443))
        updater = Updater(token=token, use_context=True)
        dispatcher = updater.dispatcher

        start_handler = CommandHandler("start", start)
        conv_handler = ConversationHandler(
            entry_points=[MessageHandler(Filters.text & (~Filters.command), start_get_video_url)],
            states={
                RESOLUTION: [MessageHandler(Filters.text, get_video)]
            },
            fallbacks=[CommandHandler("cancel", cancel)]
        )

        dispatcher.add_handler(start_handler)
        dispatcher.add_handler(conv_handler)

        updater.start_webhook(listen="0.0.0.0",
                              port=port,
                              url_path=token,
                              webhook_url="tvod.fly.dev/" + token)
        updater.idle()

if __name__ == "__main__":
    main()
